const express = require('express');

const app = express();

const port = process.env.SERVER_PORT || 5000;

app.listen(port, () => console.log(`listening on port: ${port}`))

app.get('/test', (req, res) => {
    res.send('express-node-knex-postgre server');
})